<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    //
    public function bio(){
        return view('layout.regis');
    }

    public function kirim(Request $request){
      
        $nama = $request['nama'];
        $gender = $request['gender'];
        $negara = $request['negara'];
        $alamat = $request['alamat'];
        return view('layout.wel', compact('nama','gender','negara','alamat'));

    }

}
