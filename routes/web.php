<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// 


// Route :: get('/', function(){
//     return view('layout.home');
// });  
// Route :: get('/regis', function(){
//     return view('layout.regis');
// });  
// Route :: get('/wel', function(){
//     return view('layout.wel');
// });  

Route:: get('/', 'AuthController@index');
Route:: get('/regis', 'IndexController@bio');
Route:: post('/wel', 'IndexController@kirim'); 

Route::get('/data-tables', function(){
    return view('table.data-tables');
});


